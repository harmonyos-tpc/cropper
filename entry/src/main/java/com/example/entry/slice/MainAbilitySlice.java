package com.example.entry.slice;

import com.edmodo.cropper.CropImageView;
import com.example.entry.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class MainAbilitySlice extends AbilitySlice {
    private Context context;
    private Switch btn_switch;
    private Text aspectRatioXTextView;
    private Slider aspectRatioXSeekBar;
    private Text aspectRatioYTextView;
    private Slider aspectRatioYSeekBar;
    private CropImageView cropImageView;
    private Image croppedImageView;
    private Button cropButton;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        context = getContext();
        ComponentContainer rootLayout =
                (ComponentContainer)
                        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_main, null, false);
        super.setUIContent(rootLayout);
        initView();
    }

    public void initView() {
        btn_switch = (Switch) findComponentById(ResourceTable.Id_fixedAspectRatioToggle);
        aspectRatioXTextView = (Text) findComponentById(ResourceTable.Id_aspectRatioX);
        aspectRatioXSeekBar = (Slider) findComponentById(ResourceTable.Id_aspectRatioXSeek);
        aspectRatioYTextView = (Text) findComponentById(ResourceTable.Id_aspectRatioY);
        aspectRatioYSeekBar = (Slider) findComponentById(ResourceTable.Id_aspectRatioYSeek);
        cropImageView = (CropImageView) findComponentById(ResourceTable.Id_cropImageview);
        croppedImageView = (Image) findComponentById(ResourceTable.Id_croppedImageView);
        cropButton = (Button) findComponentById(ResourceTable.Id_Button_crop);
        initSwitch();

        btn_switch.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                                        cropImageView.setFixedAspectRatio(isChecked);
                                        cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(),
                         aspectRatioYSeekBar.getProgress());
                        aspectRatioXSeekBar.setEnabled(isChecked);
                        aspectRatioYSeekBar.setEnabled(isChecked);
                    }
                });

        aspectRatioXSeekBar.setEnabled(false);
        aspectRatioYSeekBar.setEnabled(false);

        aspectRatioXTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));
        aspectRatioYTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));

        aspectRatioXSeekBar.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int progress, boolean b) {
                        if (progress < 1) {
                            aspectRatioXSeekBar.setProgressValue(1);
                        }
                                        cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(),
                         aspectRatioYSeekBar.getProgress());
                        aspectRatioXTextView.setText(String.valueOf(aspectRatioXSeekBar.getProgress()));
                    }

                    @Override
                    public void onTouchStart(Slider slider) {}

                    @Override
                    public void onTouchEnd(Slider slider) {}
                });

        aspectRatioYSeekBar.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int progress, boolean b) {
                        if (progress < 1) {
                            aspectRatioYSeekBar.setProgressValue(1);
                        }
                                        cropImageView.setAspectRatio(aspectRatioXSeekBar.getProgress(),
                         aspectRatioYSeekBar.getProgress());
                        aspectRatioYTextView.setText(String.valueOf(aspectRatioYSeekBar.getProgress()));
                    }

                    @Override
                    public void onTouchStart(Slider slider) {}

                    @Override
                    public void onTouchEnd(Slider slider) {}
                });

        cropButton.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        final PixelMap croppedImage = cropImageView.getCroppedImage();
                        croppedImageView.setPixelMap(croppedImage);
                    }
                });
    }

    private void initSwitch() {
        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFF1E90FF));
        elementThumbOn.setCornerRadius(50);

        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOff.setCornerRadius(50);

        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xFF87CEFA));
        elementTrackOn.setCornerRadius(50);

        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFF808080));
        elementTrackOff.setCornerRadius(50);

        btn_switch.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
        btn_switch.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[] {ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[] {ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[] {ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[] {ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }
}
