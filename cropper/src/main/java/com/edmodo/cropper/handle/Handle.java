package com.edmodo.cropper.handle;

import com.edmodo.cropper.customutils.RectF;
import com.edmodo.cropper.edge.Edge;

public enum Handle {
    TOP_LEFT(new CornerHandleHelper(Edge.TOP, Edge.LEFT)),
    TOP_RIGHT(new CornerHandleHelper(Edge.TOP, Edge.RIGHT)),
    BOTTOM_LEFT(new CornerHandleHelper(Edge.BOTTOM, Edge.LEFT)),
    BOTTOM_RIGHT(new CornerHandleHelper(Edge.BOTTOM, Edge.RIGHT)),
    LEFT(new VerticalHandleHelper(Edge.LEFT)),
    TOP(new HorizontalHandleHelper(Edge.TOP)),
    RIGHT(new VerticalHandleHelper(Edge.RIGHT)),
    BOTTOM(new HorizontalHandleHelper(Edge.BOTTOM)),
    CENTER(new CenterHandleHelper());

    private HandleHelper mHelper;

    Handle(HandleHelper helper) {
        mHelper = helper;
    }

    public void updateCropWindow(float x, float y, RectF imageRect, float snapRadius) {
        mHelper.updateCropWindow(x, y, imageRect, snapRadius);
    }

    public void updateCropWindow(float x, float y, float targetAspectRatio, RectF imageRect, float snapRadius) {
        mHelper.updateCropWindow(x, y, targetAspectRatio, imageRect, snapRadius);
    }
}
