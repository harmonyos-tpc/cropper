package com.edmodo.cropper.util;

import com.edmodo.cropper.ResourceTable;
import com.edmodo.cropper.customutils.ResUtil;

import ohos.agp.components.AttrHelper;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class PaintUtil {
    public static Paint newBorderPaint(Context context) {
        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(AttrHelper.vp2px(2.5f, context));
        paint.setColor(ResUtil.getNewColor(context, ResourceTable.Color_border));
        return paint;
    }

    public static Paint newGuidelinePaint(Context context) {
        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(AttrHelper.vp2px(1f, context));
        paint.setColor(ResUtil.getNewColor(context, ResourceTable.Color_guideline));
        return paint;
    }

    public static Paint newSurroundingAreaOverlayPaint(Context context) {
        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setStrokeWidth(AttrHelper.vp2px(1f, context));
        paint.setColor(new Color(Color.getIntColor("#B0000000")));
        return paint;
    }

    public static Paint newCornerPaint(Context context) {
        final Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(AttrHelper.vp2px(3.5f, context));
        paint.setColor(ResUtil.getNewColor(context, ResourceTable.Color_corner));
        return paint;
    }
}
