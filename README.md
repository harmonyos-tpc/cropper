# Gradle Dependency
```
dependencies{
   implementation 'io.openharmony.tpc.thirdlib:cropper:1.0.0'
}
```

Cropper
=======
The Cropper is an image cropping tool. It provides a way to set an image in XML and programmatically, and displays a resizable crop window on top of the image. Calling the method getCroppedImage() will then return the PixelMap marked by the crop window.

Developers can customize the following attributes (both via XML and programmatically):

- appearance of guidelines in the crop window
- whether the aspect ratio is fixed or not
- aspect ratio (if the aspect ratio is fixed)
- image resource

License
=======
Copyright 2013, Edmodo, Inc. 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Contributions
